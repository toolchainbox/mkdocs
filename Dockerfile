FROM python:3.9-alpine3.15

LABEL maintainer="sebastian.zoll@sva.de"
LABEL version="1.0"
LABEL description="\
This Dockerfile installs python3 and pip3 to\
then install and run MkDocs with several extensions."

RUN mkdir /src && \
    apk update && \
    apk add --no-cache git && \
    pip install \
        mkdocs \
        mkdocs-material \
        pymdown-extensions \
        mkdocs-section-index \
        mkdocs-git-revision-date-localized-plugin \
        mkdocs-print-site-plugin \
        mkdocs-video \
        mkdocs-material-extensions \
        git+https://github.com/jldiaz/mkdocs-plugin-tags.git \
        git+https://github.com/fralau/mkdocs_macros_plugin.git@v0.5.11
    

WORKDIR /src/
